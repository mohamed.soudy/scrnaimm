# ScRNAIMM-Replication
This repository has the scripts used for replicating the analysis of ScRNA-IMM 

## Replication 

The repo contains three main folders 
- Biological : Benchmarking the method on the biological data 
- Simulation: Benchmarking the method on the simulated data 
- DE: Differential expression analysis
 
## Benchmarking Experiments 
- Pull the docker image 
 ```
 docker pull mohmedsoudy/scrnaimp:latest
 ```
- Adjust the docker parameters
 ```
 docker run -v "D:/Missing-value-project/New-Method/docker-image/output:/app/output" -v "D:/Missing-value-project/New-Method/   docker-image/input/goolam.csv:/app/goolam.csv" -v "D:/Missing-value-project/New-Method/docker-image/input/goolam_labels.csv:/app/  goolam_labels.csv" scrnaimp:latest "/app/goolam.csv" "/app/output/" F 1 8 5 F "/app/goolam_labels.csv"
``` 
- Parameters configurations 
  - "D:/Missing-value-project/New-Method/docker-image/output:/app/output": Output directory mount folder 
  - "D:/Missing-value-project/New-Method/docker-image/input/goolam.csv:/app/goolam.csv": Input matrix were the cells in columns and the genes/transcripts in rows
  - "D:/Missing-value-project/New-Method/docker-image/input/goolam_labels.csv:/app/goolam_labels.csv": Original labels in first column
  - scrnaimp:latest: The docker image
  - "/app/goolam.csv": The mounted input matrix 
  - "/app/output/": The mounted output directory 
  - F: Boolean expression wether to perform log transformation or not 
  - 1: Number of cores 
  - 8: Method selection (1: scISR, 2: MAGIC, 3: ALRA, 4: VIPER, 5: scImpute, 6: ScRNAIMM-cells, 7: ScRNAIMM-genes, 8: All methods)
  - 5: Number of clusters 
  - F: Boolean expression wether the data is 10X or not
  - "/app/goolam_labels.csv": Original labels 
