library(edgeR)
get_de_genes <- function(counts_path, group, sep = ",", output_path){
  
  counts <- read.csv(counts_path, sep = sep, row.names = 1)
  
  dge_list <- DGEList(counts=counts, group=group)
  
  keep <- filterByExpr(dge_list)
  dge_list_filt <- dge_list[keep, , keep.lib.sizes=FALSE]
  
  dge_list_norm <- calcNormFactors(object = dge_list_filt)
  
  dge_list_fit <- estimateDisp(y = dge_list_norm)
  dge_list_pval <- exactTest(object = dge_list_fit)
  
  dge_list_fdr <- topTags(object = dge_list_pval, n = "Inf")
  dge_list_sig <- dge_list_fdr$table[dge_list_fdr$table$FDR < 0.05,]
  
  write.csv(x = dge_list_sig, file = paste0(output_path, tools::file_path_sans_ext(basename(counts_path)) , "_sig.csv"))
  return(dge_list_sig)
}

input_files <- list.files("imputation-output/input/", full.names = T)
groups <- read.csv("Dataset2/Dataset2/DEG_labels.csv")
outpath <- "results/"

for (file in input_files){
  tryCatch({
    
    dge_list <- get_de_genes(counts_path = file, group = groups$V1, output_path = outpath)
  },finally = {
    next
  }
  )
}
