# Create two example sets
pd_genes <- c("GeneA", "GeneB", "GeneC", "GeneD", "GeneE")
ad_genes <- c("GeneB", "GeneD", "GeneF", "GeneG", "GeneH")

fisher_test <- function(pd_genes, ad_genes){
  # Calculate the size of each set
  n_pd <- length(pd_genes)
  n_ad <- length(ad_genes)
  
  # Calculate the size of the overlap (intersection) between the sets
  overlap <- length(intersect(pd_genes, ad_genes))
  
  # Perform Fisher's Exact Test
  fisher_test_result <- fisher.test(matrix(c(overlap, n_pd - overlap, n_ad - overlap, 0), nrow = 2))
  
  # Print the results
  cat("Overlap between pd and ad:", overlap, "\n")
  cat("Fisher's Exact Test p-value:", fisher_test_result$p.value, "\n")
  
  # Interpret the results
  if (fisher_test_result$p.value < 0.05) {
    cat("The overlap is statistically significant (p-value < 0.05).\n")
  } else {
    cat("The overlap is not statistically significant (p-value >= 0.05).\n")
  }
  return(overlap, fisher_test_result$p.value)
}

g_test <- function(pd_genes, ad_genes){
  
  if (!require(vcd)) {
    install.packages("vcd")
    library(vcd)
  }
  
  contingency_table <- table(factor(pd_genes, levels = unique(c(pd_genes, ad_genes))),
                             factor(ad_genes, levels = unique(c(pd_genes, ad_genes))))
  
  # Perform the G-test
  g_test_result <- g.test(contingency_table)
  
  cat("p-value:", g_test_result$p, "\n")
  
  # Check for statistical significance at a specific alpha level (e.g., 0.05)
  alpha <- 0.05
  if (g_test_result$p < alpha) {
    cat("There is a statistically significant overlap between the gene sets.\n")
  } else {
    cat("There is no statistically significant overlap between the gene sets.\n")
  }
}

fisher_test(pd_genes, ad_genes)
g_test(pd_genes, ad_genes)
