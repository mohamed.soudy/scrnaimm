#!/bin/bash -i
#SBATCH --job-name=simulation_EH5403_singularity
#SBATCH --time=14-00:00:00
#SBATCH --mail-user=Mohamed.soudy@uni.lu
#SBATCH --mail-type=ALL 
#SBATCH --output=singularity_logs/simulation_EH5403_singularity.out	
#SBATCH --error=singularity_logs/simulation_EH5403_singularity.%J.stdout
#SBATCH --output=singularity_logs/simulation_EH5403_singularity.%J.stderr
#SBATCH -p bigmem
#SBATCH -c 48

cd /work/projects/imputation/Simulations/
module load tools/Singularity/3.8.1
singularity run -B "results/EH5403:/app/output" -B "datasets/EH5403.csv:/app/EH5403.csv" -B "metadata/EH5403_labels.csv:/app/EH5403_labels.csv" /home/users/msoudy/Imputation-New-Run/singularity-image/scrnaimp.sif "/app/EH5403.csv" "/app/output/" F 1 8 7 T "/app/EH5403_labels.csv" 
