#!/bin/bash -i
#SBATCH --job-name=Slyper-viper
#SBATCH --time=14-00:00:00
#SBATCH --mail-user=Mohamed.soudy@uni.lu
#SBATCH --mail-type=ALL 
#SBATCH --output=Imputation-Slyper-viper.out	
#SBATCH --error=Imputation-Slyper-viper.%J.stdout
#SBATCH --output=Imputation-Slyper-viper.%J.stderr
#SBATCH -p bigmem
#SBATCH -c 48  
cd /home/users/msoudy/Imputation-New-Run/
conda activate rstudio-env
Rscript --no-save --no-restore --vanilla ImputationScripts/Slyper-viper.R datasets/csv/slyper.csv New-Output/ 100
